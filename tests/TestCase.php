<?php

namespace Test;


use Faker\Factory;
use Illuminate\Http\Request;
use MiamiOH\AuthMan\AuthManServiceProvider;
use MiamiOH\AuthMan\Authorization;
use Orchestra\Testbench\TestCase as OrchestraTestCase;

class TestCase extends OrchestraTestCase
{
    /**
     * @var \Faker\Generator
     */
    protected $faker;

    public function setUp(): void
    {
        parent::setUp();

        $this->faker = Factory::create();
    }

    protected function getPackageProviders($app)
    {
        return [AuthManServiceProvider::class];
    }

    protected function getPackageAliases($app)
    {
        return [];
    }

    protected function makeRequest(string $uri = 'https://example.com',
                                   string $method = 'GET',
                                   array $parameters = [],
                                   array $cookies = [],
                                   array $files = [],
                                   array $server = [],
                                   $content = null): Request
    {
        return Request::create($uri, $method, $parameters, $cookies, $files, $server, $content);
    }

    protected function makeJsonRequest(string $uri = 'https://example.com',
                                       string $method = 'GET',
                                       array $parameters = [],
                                       array $cookies = [],
                                       array $files = [],
                                       array $server = [],
                                       $content = null): Request
    {
        $server['HTTP_ACCEPT'] = 'application/json';

        return $this->makeRequest($uri, $method, $parameters, $cookies, $files, $server, $content);
    }

    protected function makeAuthorizationData(array $overrides = []): array
    {
        return array_merge([
                'applicationName' => $this->faker->word(),
                'module' => $this->faker->word(),
                'key' => $this->faker->word(),
                'message' => $this->faker->sentence(5),
                'allowed' => $this->faker->boolean(),
            ], $overrides);
    }

    protected function makeAuthorization(array $overrides = []): Authorization
    {
        $data = $this->makeAuthorizationData($overrides);

        return new Authorization(
            $data['applicationName'],
            $data['module'],
            $data['key'],
            $data['allowed']
        );
    }
}
