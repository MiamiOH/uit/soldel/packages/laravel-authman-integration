<?php

namespace Test;

use Carbon\Carbon;
use Illuminate\Http\Request;
use MiamiOH\AuthMan\Exceptions\InvalidTokenException;
use MiamiOH\AuthMan\Exceptions\NoAuthorizationHeaderException;
use MiamiOH\AuthMan\TokenService;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\ResponseData;
use PHPUnit\Framework\MockObject\MockObject;
use Psr\Http\Message\RequestInterface;

class TokenServiceTest extends TestCase
{
    /** @var TokenService  */
    private $tokenService;

    /** @var Agent|MockObject  */
    private $agent;

    public function setUp(): void
    {
        parent::setUp();

        $this->agent = $this->createMock(Agent::class);

        $this->tokenService = new TokenService($this->agent);
    }

    /**
     * @dataProvider authorizationHeaderValueChecks
     */
    public function testRetrievesTokenExtractedFromHttpRequest(string $headerValue, string $expected): void
    {
        $request = Request::create('https://example.com', 'GET', [], [], [], ['HTTP_AUTHORIZATION' => $headerValue]);

        $expiration = Carbon::now('America/New_York')->addHour();

        $data = [
            'token' => $expected,
            'username' => 'test-user',
            'credential_source' => 'local',
            'expiration_time' => $expiration->toDateTimeLocalString(),
        ];

        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (RequestInterface $request) use ($expected) {
                $this->assertEquals(TokenService::AUTHENTICATION_RESOURCE . '/' . $expected, $request->getUri()->getPath());
                return true;
            }))
            ->willReturn(new ResponseData(200, $data));

        $token = $this->tokenService->resolveTokenFromRequest($request);

        $this->assertEquals($data['token'], $token->token());
        $this->assertEquals($data['username'], $token->username());
        $this->assertEquals($data['credential_source'], $token->credentialSource());
        $this->assertTrue($expiration->isSameSecond($token->expirationTime()));
    }

    public static function authorizationHeaderValueChecks(): array
    {
        return [
            'bare value' => ['abc123', 'abc123'],
            'alt bare value' => ['xyz789', 'xyz789'],
            'bearer token' => ['bearer abc123', 'abc123'],
            'token label' => ['token abc123', 'abc123'],
        ];
    }

    public function testThrowsExceptionExtractingTokenFromHttpRequestWithNoAuthorizationHeader(): void
    {
        $request = Request::create('https://example.com', 'GET');

        $this->expectException(NoAuthorizationHeaderException::class);

        $this->tokenService->resolveTokenFromRequest($request);
    }

    public function testThrowsExceptionResolvingInvalidToken(): void
    {
        $request = Request::create('https://example.com', 'GET', [], [], [], ['HTTP_AUTHORIZATION' => 'bearer abc123']);


        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (RequestInterface $request) {
                $this->assertEquals(TokenService::AUTHENTICATION_RESOURCE . '/abc123', $request->getUri()->getPath());
                return true;
            }))
            ->willReturn(new ResponseData(404));

        $this->expectException(InvalidTokenException::class);

        $this->tokenService->resolveTokenFromRequest($request);
    }
}
