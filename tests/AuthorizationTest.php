<?php

namespace Test;

use MiamiOH\AuthMan\Authorization;

class AuthorizationTest extends TestCase
{
    public function testReturnsApplication(): void
    {
        $name = $this->faker->word();

        $item = $this->makeAuthorization(['applicationName' => $name]);

        $this->assertEquals($name, $item->application());
    }

    public function testReturnsCategory(): void
    {
        $name = $this->faker->word();

        $item = $this->makeAuthorization(['module' => $name]);

        $this->assertEquals($name, $item->category());
    }

    public function testReturnsKey(): void
    {
        $key = $this->faker->word();

        $item = $this->makeAuthorization(['key' => $key]);

        $this->assertEquals($key, $item->key());
    }

    public function testReturnsAllowed(): void
    {
        $allowed = $this->faker->boolean();

        $item = $this->makeAuthorization(['allowed' => $allowed]);

        $this->assertEquals($allowed, $item->allowed());
    }

    public function testCanBeCreatedFromDataArray(): void
    {
        $authorization = Authorization::fromDataArray([
            'applicationName' => 'IAM-Portal',
            'module' => 'Roles',
            'key' => 'admin',
            'message' => '',
            'allowed' => 'false'
        ]);

        $this->assertEquals('IAM-Portal', $authorization->application());
        $this->assertEquals('Roles', $authorization->category());
        $this->assertEquals('admin', $authorization->key());
    }

    /**
     * @dataProvider dataArrayAllowedChecks
     */
    public function testProducesCorrectAllowValueWhenCreatedFromDataArray(string $allowed, bool $expected): void
    {
        $authorization = Authorization::fromDataArray([
            'applicationName' => 'IAM-Portal',
            'module' => 'Roles',
            'key' => 'admin',
            'message' => '',
            'allowed' => $allowed
        ]);

        $this->assertEquals($expected, $authorization->allowed());
    }

    public static function dataArrayAllowedChecks(): array
    {
        return [
            'string false' => ['false', false],
            'string true' => ['true', true],
        ];
    }
}
