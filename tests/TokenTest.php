<?php

namespace Test;

use Carbon\Carbon;
use MiamiOH\AuthMan\Token;

class TokenTest extends TestCase
{
    /** @var Token  */
    private $token;

    /** @var Carbon  */
    private $expiration;

    public function setUp(): void
    {
        parent::setUp();

        $this->expiration = Carbon::now()->addHour();
        $this->token = new Token('abc123', 'my_user', 'local', $this->expiration);
    }

    public function testCanBeConstructedFromResponseData(): void
    {
        $expiration = Carbon::now('America/New_York')->addHour();

        $data = [
            'token' => 'abc123',
            'username' => 'test-user',
            'credential_source' => 'local',
            'expiration_time' => $expiration->toDateTimeLocalString(),
        ];

        $token = Token::fromResponseData($data);

        $this->assertEquals('abc123', $token->token());
        $this->assertEquals('test-user', $token->username());
        $this->assertEquals('local', $token->credentialSource());
        $this->assertTrue($expiration->isSameSecond($token->expirationTime()));
    }

    public function testReturnsTokenValue(): void
    {
        $this->assertEquals('abc123', $this->token->token());
    }

    public function testReturnsUsername(): void
    {
        $this->assertEquals('my_user', $this->token->username());
    }

    public function testReturnsCredentialSource(): void
    {
        $this->assertEquals('local', $this->token->credentialSource());
    }

    public function testReturnsExpirationTime(): void
    {
        $this->assertEquals($this->expiration, $this->token->expirationTime());
    }

    /**
     * @dataProvider assertIsValidChecks
     */
    public function testAssertsIsValid(Carbon $expiration, bool $expected): void
    {
        $data = [
            'token' => 'abc123',
            'username' => 'test-user',
            'credential_source' => 'local',
            'expiration_time' => $expiration->toDateTimeLocalString(),
        ];

        $token = Token::fromResponseData($data);

        $this->assertEquals($expected, $token->isValid());
        $this->assertEquals(!$expected, $token->isExpired());
    }

    public static function assertIsValidChecks(): array
    {
        return [
            'future' => [Carbon::now('America/New_York')->addHour(), true],
            'past' => [Carbon::now('America/New_York')->subHours(2), false],
        ];
    }
}
