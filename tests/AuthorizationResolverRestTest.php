<?php

namespace Test;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MiamiOH\AuthMan\AuthorizationResolverRest;
use MiamiOH\AuthMan\DataParser;
use MiamiOH\AuthMan\Exceptions\ClientException;
use MiamiOH\AuthMan\Exceptions\ItemNotFoundException;
use MiamiOH\RESTng\Client\Agent;
use MiamiOH\RESTng\Client\ResponseData;
use PHPUnit\Framework\MockObject\MockObject;

class AuthorizationResolverRestTest extends TestCase
{
    /**
     * @var AuthorizationResolverRest
     */
    private $resolver;

    /**
     * @var Agent|MockObject
     */
    private $agent;

    public function setUp(): void
    {
        parent::setUp();

        $this->agent = $this->createMock(Agent::class);

        $this->resolver = new AuthorizationResolverRest($this->agent, new DataParser(), 30);
    }

    /**
     * @dataProvider authorizationChecks
     */
    public function testReturnsAuthorization(string $key, bool $expected): void
    {
        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getPath());
                $this->assertStringContainsString('doej', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200, [
                $this->makeAuthorizationData(['key' => 'admin', 'allowed' => false]),
                $this->makeAuthorizationData(['key' => 'user', 'allowed' => true]),
            ]));

        $authorization = $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', $key);

        $this->assertEquals($expected, $authorization->allowed());
    }

    public static function authorizationChecks(): array
    {
        return [
            'admin not allowed' => ['admin', false],
            'user allowed' => ['user', true],
        ];
    }

    public function testThrowsExceptionWhenRequestedKeyIsNotFoundInCategory(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->willReturn(new ResponseData(200, [$this->makeAuthorizationData(['key' => 'admin'])]));

        $this->expectException(ItemNotFoundException::class);

        $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'user');
    }

    public function testReturnsAuthorizationsForCategory(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getPath());
                $this->assertStringContainsString('doej', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200, [
                $this->makeAuthorizationData(['key' => 'admin', 'allowed' => 'false']),
                $this->makeAuthorizationData(['key' => 'user', 'allowed' => 'true']),
            ]));

        $authorizations = $this->resolver->getAuthorizations('doej', 'MyApplication', 'MyCategory');

        $this->assertCount(2, $authorizations);
    }

    public function testUsesAgentToGetKey(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getPath());
                $this->assertStringContainsString('doej', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200, [$this->makeAuthorizationData(['key' => 'admin', 'allowed' => 'true'])]));

        $authorization = $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'admin');

        $this->assertEquals(true, $authorization->allowed());
    }

    public function testUsesCachedCategoryDataWhenPresent(): void
    {
        Cache::shouldReceive('has')->once()
            ->with($this->resolver->categoryCacheKey('doej', 'MyApplication', 'MyCategory'))
            ->andReturn(true);

        Cache::shouldReceive('remember')->never();

        Cache::shouldReceive('get')->once()
            ->with($this->resolver->categoryCacheKey('doej', 'MyApplication', 'MyCategory'))
            ->andReturn(new Collection([
                'admin' => $this->makeAuthorization(['key' => 'admin', 'allowed' => 'true']),
            ]));

        $this->agent->expects($this->never())->method('run');

        $authorization = $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'admin');

        $this->assertEquals(true, $authorization->allowed());
    }

    public function testThrowsExceptionWhenHttpRequestHasError(): void
    {
        $this->agent->expects($this->once())->method('run')
            ->willReturn(new ResponseData(500));

        $this->expectException(ClientException::class);

        $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'admin');
    }

    public function testGetsCategoryDataWithoutCache(): void
    {
        Cache::shouldReceive('has')->never();

        Cache::shouldReceive('remember')->never();

        Cache::shouldReceive('get')->never();

        $this->agent->expects($this->once())->method('run')
            ->with($this->callback(function (Request $request) {
                $this->assertStringContainsString('MyApplication', $request->getUri()->getPath());
                $this->assertStringContainsString('MyCategory', $request->getUri()->getPath());
                $this->assertStringContainsString('doej', $request->getUri()->getQuery());
                return true;
            }))
            ->willReturn(new ResponseData(200, [$this->makeAuthorizationData(['key' => 'admin', 'allowed' => 'true'])]));

        $authorization = $this->resolver->withoutCache()
            ->getAuthorization('doej', 'MyApplication', 'MyCategory', 'admin');

        $this->assertEquals(true, $authorization->allowed());
    }
}
