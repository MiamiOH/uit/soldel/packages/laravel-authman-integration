<?php

namespace Test;

use MiamiOH\AuthMan\AuthorizationResolverArray;
use MiamiOH\AuthMan\Exceptions\ItemNotFoundException;

class AuthorizationResolverArrayTest extends TestCase
{
    /**
     * @var AuthorizationResolverArray
     */
    private $resolver;

    public function setUp(): void
    {
        parent::setUp();

        $this->resolver = new AuthorizationResolverArray();
    }

    /**
     * @dataProvider authorizationChecks
     */
    public function testReturnsAuthorization(string $key, bool $expected): void
    {
        $this->resolver->withAuthorizations([
            'MyApplication' => [
                'MyCategory' => [
                    ['user', true],
                    ['admin', false],
                ]
            ]
        ]);

        $authorization = $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', $key);

        $this->assertEquals($expected, $authorization->allowed());
    }

    public static function authorizationChecks(): array
    {
        return [
            'admin not allowed' => ['admin', false],
            'user allowed' => ['user', true],
        ];
    }

    public function testThrowsExceptionWhenRequestedKeyIsNotFoundInCategory(): void
    {
        $this->resolver->withAuthorizations([
            'MyApplication' => [
                'MyCategory' => [
                    ['admin', true],
                ]
            ]
        ]);

        $this->expectException(ItemNotFoundException::class);

        $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'user');
    }

    public function testReturnsAuthorizationsForCategory(): void
    {
        $this->resolver->withAuthorizations([
            'MyApplication' => [
                'MyCategory' => [
                    ['user', true],
                    ['admin', false],
                ]
            ]
        ]);

        $authorizations = $this->resolver->getAuthorizations('doej', 'MyApplication', 'MyCategory');

        $this->assertCount(2, $authorizations);
    }

    public function testAllowsAllAuthorizations(): void
    {
        $this->resolver->withAllAuthorizations();

        $auth = $this->resolver->getAuthorization('doej', 'MyApplication', 'MyCategory', 'user');

        $this->assertTrue($auth->allowed());
    }
}
