<?php

namespace Test\Middleware;

use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use MiamiOH\AuthMan\AuthorizationService;
use MiamiOH\AuthMan\Exceptions\NoAuthorizationHeaderException;
use MiamiOH\AuthMan\Middleware\MiamiTokenAuthorization;
use MiamiOH\AuthMan\Token;
use MiamiOH\AuthMan\TokenService;
use PHPUnit\Framework\MockObject\MockObject;
use Test\TestCase;

class MiamiTokenAuthorizationTest extends TestCase
{
    /** @var MiamiTokenAuthorization */
    private $middleware;

    /** @var Guard|MockObject */
    private $guard;

    /** @var TokenService|MockObject  */
    private $tokenService;

    /** @var AuthorizationService|MockObject  */
    private $authorizationService;

    /** @var Request  */
    private $request;

    /** @var callable */
    private $next;
    /** @var bool */
    private $nextMiddlewareWasCalled;

    public function setUp(): void
    {
        parent::setUp();

        $this->enableNextMiddleware();

        $this->request = $this->makeJsonRequest();

        $this->guard = $this->createMock(Guard::class);
        $this->tokenService = $this->createMock(TokenService::class);
        $this->authorizationService = $this->createMock(AuthorizationService::class);

        $this->middleware = new MiamiTokenAuthorization($this->guard, $this->tokenService, $this->authorizationService);
    }

    public function testFailsAuthenticationWhenTokenIsNotPresent(): void
    {
        $this->tokenService->expects($this->once())->method('resolveTokenFromRequest')
            ->with($this->equalTo($this->request))
            ->willThrowException(new NoAuthorizationHeaderException());

        $response = $this->handleRequest();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->status());
        $this->assertFalse($this->nextMiddlewareWasCalled);
    }

    public function testFailsAuthenticationWhenTokenIsExpired(): void
    {
        $this->tokenService->expects($this->once())->method('resolveTokenFromRequest')
            ->with($this->equalTo($this->request))
            ->willReturn($this->makeToken(['expiration_time' => Carbon::now()->subHours(2)]));

        $this->authorizationService->method('isAuthorizedAny')->willReturn(true);

        $response = $this->handleRequest();

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->status());
        $this->assertFalse($this->nextMiddlewareWasCalled);
    }

    public function testCallsNextMiddlewareWhenUserIsAuthorizedForAtLeastOneKey(): void
    {
        $username = 'doej';
        $applicationName = 'Test Application';
        $moduleName = 'Test Module';
        $keyList = ['view'];

        $this->tokenService->expects($this->once())->method('resolveTokenFromRequest')
            ->with($this->equalTo($this->request))
            ->willReturn($this->makeToken(['username' => $username]));

        $this->authorizationService->method('fromApplication')
            ->with($this->equalTo($applicationName))
            ->willReturnSelf();

        $this->authorizationService->method('fromCategory')
            ->with($this->equalTo($moduleName))
            ->willReturnSelf();

        $this->authorizationService->method('isAuthorizedAny')
            ->with(
                $this->equalTo($username),
                $this->equalTo($keyList),
            )
            ->willReturn(true);

        $response = $this->handleRequest(
            ['applicationName' => $applicationName, 'moduleName' => $moduleName, 'keyList' => implode(',',$keyList)]
        );

        $this->assertEquals(Response::HTTP_OK, $response->status());
        $this->assertTrue($this->nextMiddlewareWasCalled);
    }

    public function testFailsAuthenticationWhenUserIsAuthorizedForAtLeastOneKey(): void
    {
        $username = 'doej';
        $applicationName = 'Test Application';
        $moduleName = 'Test Module';
        $keyList = ['view'];

        $this->tokenService->expects($this->once())->method('resolveTokenFromRequest')
            ->with($this->equalTo($this->request))
            ->willReturn($this->makeToken(['username' => $username]));

        $this->authorizationService->method('isAuthorizedAny')
            ->with(
                $this->equalTo($username),
                $this->equalTo($applicationName),
                $this->equalTo($moduleName),
                $this->equalTo($keyList),
            )
            ->willReturn(false);

        $response = $this->handleRequest(
            ['applicationName' => $applicationName, 'moduleName' => $moduleName, 'keyList' => implode(',',$keyList)]
        );

        $this->assertEquals(Response::HTTP_UNAUTHORIZED, $response->status());
        $this->assertFalse($this->nextMiddlewareWasCalled);
    }

    public function testClearsAuthenticatedTokenUserOnRequestStart(): void
    {
        $this->authorizationService->expects($this->once())->method('clearAuthenticatedTokenUser');

        $this->handleRequest();
    }

    public function testSetsAuthenticatedTokenUserForAuthenticatedRequest(): void
    {
        $this->tokenService->method('resolveTokenFromRequest')
            ->willReturn($this->makeToken(['username' => 'test-token-user']));

        $this->authorizationService->method('fromApplication')->willReturnSelf();

        $this->authorizationService->method('fromCategory')->willReturnSelf();

        $this->authorizationService->method('isAuthorizedAny')->willReturn(true);

        $this->authorizationService->expects($this->once())->method('setAuthenticatedTokenUser')
            ->with('test-token-user');

        $this->handleRequest();
    }

    private function handleRequest(array $overrides = []): Response
    {
        $options = array_merge([
            'applicationName' => 'My Application',
            'moduleName' => 'My Module',
            'keyList' => 'view|create',
            'required' => 'true',
        ], $overrides);

        return $this->middleware->handle($this->request,
            $this->next,
            $options['applicationName'],
            $options['moduleName'],
            $options['keyList'],
            $options['required']);
    }

    private function enableNextMiddleware(): void
    {
        $this->nextMiddlewareWasCalled = false;

        $this->next = function (Request $nextRequest) {
            $this->assertSame($this->request, $nextRequest);
            $this->nextMiddlewareWasCalled = true;
            return new Response(null, Response::HTTP_OK);
        };
    }

    private function makeTokenData(array $overrides = []): array
    {
        return array_merge([
            'token' => 'abc123',
            'username' => 'test-user',
            'credential_source' => 'local',
            'expiration_time' => Carbon::now()->addHour(),
        ], $overrides);
    }

    private function makeToken(array $overrides = []): Token
    {
        return Token::fromResponseData($this->makeTokenData($overrides));
    }
}
