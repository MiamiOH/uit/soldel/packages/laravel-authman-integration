<?php

namespace Test;

use Illuminate\Config\Repository;
use Illuminate\Support\Collection;
use MiamiOH\AuthMan\Authorization;
use MiamiOH\AuthMan\AuthorizationResolver;
use MiamiOH\AuthMan\AuthorizationService;
use MiamiOH\AuthMan\Exceptions\ItemNotFoundException;
use PHPUnit\Framework\MockObject\MockObject;


class AuthorizationServiceTest extends TestCase
{
    /**
     * @var AuthorizationResolver|MockObject
     */
    private $resolver;
    /**
     * @var Repository|MockObject
     */
    private $appConfig;

    private $application;
    private $category;

    public function setUp(): void
    {
        parent::setUp();

        $this->resolver = $this->createMock(AuthorizationResolver::class);
        $this->appConfig = $this->createMock(Repository::class);

        $this->appConfig->method('get')
            ->willReturnMap([
                ['authman.default-application', null, 'My Application'],
                ['authman.default-category', null, 'My Category'],
            ]);

        $this->application = $this->faker->word();
        $this->category = $this->faker->word();
    }

    public function testUsesDefaultApplicationAndCategoryWhenNotGiven(): void
    {
        $service = new AuthorizationService($this->resolver, $this->appConfig);

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('testuser'),
                $this->equalTo('My Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeAuthorization());

        $service->isAuthorized('testuser', 'test-key');
    }

    public function testUsesDefaultApplicationAndCategoryWhenNull(): void
    {
        $service = new AuthorizationService($this->resolver, $this->appConfig, null, null);

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('testuser'),
                $this->equalTo('My Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeAuthorization());

        $service->isAuthorized('testuser', 'test-key');
    }

    public function testUsesDefaultCategoryWhenNotGiven(): void
    {
        $service = new AuthorizationService($this->resolver, $this->appConfig, 'Test Application');

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('testuser'),
                $this->equalTo('Test Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeAuthorization());

        $service->isAuthorized('testuser', 'test-key');
    }

    public function testUsesDefaultCategoryWhenNull(): void
    {
        $service = new AuthorizationService($this->resolver, $this->appConfig, 'Test Application', null);

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('testuser'),
                $this->equalTo('Test Application'),
                $this->equalTo('My Category'),
                $this->equalTo('test-key')
            )->willReturn($this->makeAuthorization());

        $service->isAuthorized('testuser', 'test-key');
    }

    public function testGetsAuthorizationFromResolver(): void
    {
        $service = $this->makeAuthorizationService();

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo($this->application),
                $this->equalTo($this->category),
                $this->equalTo('admin')
            )
            ->willReturn(new Authorization($this->application, $this->category, 'admin', true));

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(true, $authorized);
    }

    public function testGetsAuthorizationValueFromResolverWithGivenApplication(): void
    {
        $service = $this->makeAuthorizationService()->fromApplication('Other Application');

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo('Other Application'),
                $this->equalTo($this->category),
                $this->equalTo('admin')
            )
            ->willReturn(new Authorization('Other Application', $this->category, 'admin', true));

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(true, $authorized);
    }

    public function testGetsAuthorizationValueFromResolverWithGivenCategory(): void
    {
        $service = $this->makeAuthorizationService()->fromCategory('Other Category');

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo($this->application),
                $this->equalTo('Other Category'),
                $this->equalTo('admin')
            )
            ->willReturn(new Authorization($this->application, 'Other Category', 'admin', true));

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(true, $authorized);
    }

    public function testReturnsFalseIfItemNotFound(): void
    {
        $service = $this->makeAuthorizationService()->fromApplication('Other Application');

        $this->resolver->method('getAuthorization')
            ->willThrowException(new ItemNotFoundException());

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(false, $authorized);
    }

    public function testReturnsKeysForCategory(): void
    {
        $service = $this->makeAuthorizationService();

        $this->resolver->expects($this->once())->method('getAuthorizations')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                new Authorization($this->application, $this->category, 'admin', true)
            ]));

        $keys = $service->getAuthorizedKeys('doej');

        $this->assertEquals(['admin'], $keys);
    }

    public function testReturnsOnlyAllowedAuthorizedKeysForCategory(): void
    {
        $service = $this->makeAuthorizationService();

        $this->resolver->expects($this->once())->method('getAuthorizations')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                new Authorization($this->application, $this->category, 'admin', false),
                new Authorization($this->application, $this->category, 'user', true),
            ]));

        $keys = $service->getAuthorizedKeys('doej');

        $this->assertEquals(['user'], $keys);
    }

    /**
     * @dataProvider authorizedAnyChecks
     */
    public function testChecksMultipleKeysForAuthorization(bool $admin, bool $user): void
    {
        $expected = $admin || $user;

        $service = $this->makeAuthorizationService();

        $this->resolver->expects($this->once())->method('getAuthorizations')
            ->with(
                $this->equalTo('doej'),
                $this->equalTo($this->application),
                $this->equalTo($this->category)
            )
            ->willReturn(new Collection([
                new Authorization($this->application, $this->category, 'admin', $admin),
                new Authorization($this->application, $this->category, 'user', $user),
            ]));

        $allowed = $service->isAuthorizedAny('doej', ['admin', 'user']);

        $this->assertEquals($expected, $allowed);
    }

    public static function authorizedAnyChecks(): array
    {
        return [
            'admin' => [true, false],
            'user' => [false, true],
            'both' => [true, true],
            'none' => [false, false],
        ];
    }

    public function testGetsAuthorizationFromResolverUsingCache(): void
    {
        $service = $this->makeAuthorizationService();

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->willReturn(new Authorization($this->application, $this->category, 'admin', true));

        $this->resolver->expects($this->never())->method('withoutCache');

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(true, $authorized);
    }

    public function testGetsAuthorizationFromResolverWithoutUsingCache(): void
    {
        $service = $this->makeAuthorizationService()->withoutCache();

        $this->resolver->expects($this->once())->method('getAuthorization')
            ->willReturn(new Authorization($this->application, $this->category, 'admin', true));

        $this->resolver->expects($this->once())->method('withoutCache')
            ->willReturn($this->resolver);

        $authorized = $service->isAuthorized('doej', 'admin');

        $this->assertEquals(true, $authorized);
    }

    public function testSetsAndReturnsAuthenticatedTokenUser(): void
    {
        $service = $this->makeAuthorizationService();

        $service->setAuthenticatedTokenUser('doej');

        $this->assertEquals('doej', $service->authenticatedTokenUser());
    }

    public function testClearsAuthenticatedTokenUser(): void
    {
        $service = $this->makeAuthorizationService();

        $service->setAuthenticatedTokenUser('doej');
        $service->clearAuthenticatedTokenUser();

        $this->assertNull($service->authenticatedTokenUser());
    }

    private function makeAuthorizationService(): AuthorizationService
    {
        return new AuthorizationService(
            $this->resolver,
            $this->appConfig,
            $this->application,
            $this->category
        );
    }
}
