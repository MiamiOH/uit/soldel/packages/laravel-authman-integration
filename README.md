# Authman Integration Package for Laravel 8+

## Installation

The minimum supported version is Laravel 8.

Make sure that the satis repo has been added to your composer.json file:

```
    "repositories": [
        {
            "type": "composer",
            "url": "https://satis.itapps.miamioh.edu/miamioh"
        }
    ]
```

Afterwards, run this in your app directory:

```
composer require miamioh/laravel-authman-integration
```

Optional: Publish the config file by running:

```
php artisan vendor:publish --provider 'MiamiOH\AuthMan\AuthManServiceProvider'
```

## Upgrading from 2.x to 3.x

When upgrading from 2.x to 3.x, please be aware of the following changes:

### Configuration file overhaul (Impact: High)

The `config/authman.php` file has been completely overhauled. You should delete this file. It is not necessary to publish the new configuration file. All options can be set through env variables. See the `Configuration` sections below.

### Middleware "required" indicator removed (Impact: High)

The previous version of the middleware accepted a parameter indicating a token was `required` for the covered routes. This parameter has been removed. The middleware should only be applied to routes which require token authentication.

### Middleware overhaul (Impact: Low)

The middleware class was refactored to extract the token handling into a separate component. While this and other logic changes should not have any noticeable impact, the class has been moved to the `Middleware` namespace and renamed. Use the following full classname when registering the middleware:

```
\MiamiOH\LaravelAuthMan\Middleware\MiamiTokenAuthorization::class
```

### Service provider class change (Impact: Low)

The package service provider class has been renamed and the namespace changed. If you previously added the provider to your `config/app.php` providers list, you will need to remove it. Ensure the following provider entry is not present:

 ```php
 'providers' => [
    ...
    MiamiOH\LaravelAuthMan\LaravelAuthManServiceProvider::class,
 ]
 ```

## Configuration

### Environment Configuration

The following can be set in your `.env`:

```
AUTHMAN_DEFAULT_APPLICATION="My Application"
AUTHMAN_DEFAULT_CATEGORY="My Category"
AUTHMAN_CACHE_SECONDS=300
```

The default application and category are used when the `fromApplication()` and `fromCategory()` methods do not specify an alternative.

The cache seconds controls how long resolved authorizations are cached.

### RESTng Agent Configuration

This package relies on the [PHP RESTng Client](https://gitlab.com/MiamiOH/uit/soldel/packages/restng-php-client) package for the underlying HTTP request handling. The `AuthorizationResolver` class takes a `\MiamiOH\RESTng\Client\Agent` object as a dependency and uses it to call the RESTng authorization resource. You will need to ensure a suitable `Agent` object can be resolved by the Laravel container. In the simplest form, this can be done by ensuring a valid `Endpoint` object is available:

```php
$this->app->bind(\MiamiOH\RESTng\Client\Endpoint::class, function () {
    return new \MiamiOH\RESTng\Client\Endpoint(
        config('restng.restngUrl'),
        config('restng.credentials.token.default.username'),
        config('restng.credentials.token.default.password'),
    );
});
```

**Note**: Use the proper `config()` values based on your application's configuration.

## Usage

The primary usage is via the `AuthorizationService` class. You should inject or otherwise resolve this from the Laravel container.

### Specifying an Application and Category

Authorizations are tied to an application and category in CAM (Configuration and Authorization Manager). You must specify the application and category when using this package.

```php
public function handle(\MiamiOH\AuthMan\AuthorizationService $authorizationService): void
{
    $auth = $authorizationService
                ->fromApplication('My Application')
                ->fromCategory('My Category');
}
```

Note that the `fromApplication()` and `fromCategory()` methods return a new instance of the `AuthorizationService` and the original object is unaltered. This allows the fluent invocation shown above but also means the original object should be used with care as it may not be configured as expected.

While the package supports multiple applications and categories, you may find that most authorizations are performed against a single application and category. You can set a default application and category in your environment and skip calling `fromApplication()` and `fromCategory()`:

```bash
# in the .env file:
AUTHMAN_DEFAULT_APPLICATION="My Application"
AUTHMAN_DEFAULT_CATEGORY="My Category"
```

For brevity, the remainder of this documentation assumes you are using default application and category values. If needed, you may use the `fromApplication()` and `fromCategory()` methods with any of the package methods.

### Checking Authorizations

The basic usage of the service is to evaluate authorizations for a username. Authorizations are checked with the `isAuthorized()` or `isAuthorizedAny()` methods.

```php
/** @var \MiamiOH\AuthMan\AuthorizationService $auth */
$auth->isAuthorized('username', 'key');
$auth->isAuthorizedAny('username', ['key1', 'key2']);
```

The `isAuthorized()` method accepts a username as the first argument and a single key as the second. It returns `true` if the user is authorized for that key, `false` otherwise. 

The `isAuthorizedAny()` method also accepts a username as the first argument, but accepts an array of keys as the second argument. It returns `true` if the user is authorized for at least one of the listed keys, `false` otherwise. Note that the `isAuthorizedAny()` method stops evaluating on the first authorized key and does not expose which key or keys are authorized.

A complete example might look something like this:

```php
public function handle(\MiamiOH\AuthMan\AuthorizationService $authorizationService): void
{
    // Check a single key
    $authorizedAdmin = $authorizationService->isAuthorized('doej', 'admin');

    // Check multiple keys
    $authorizedAny = $authorizationService->isAuthorizedAny('doej', ['admin', 'user']);
}
```

### Listing Authorized Keys

In some cases, you may find it helpful to get a list of keys for which a user is authorized. You can do this with the `getAuthorizedKeys()` method.

```php
/** @var \MiamiOH\AuthMan\AuthorizationService $auth */
$keys = $auth->getAuthorizedKeys('username');
```

The method returns an array of keys for which the given user is authorized.

### Authorization Middleware

This package provides middleware which can be used to perform token based authentication and authorization. The package uses the Miami custom token service to validate the token and acquire the associated username.

Register the middleware in your `app/Http/Kernel.php` file:

```php
protected $routeMiddleware = [
    ...
    'authman.miamioh' => \MiamiOH\AuthMan\Middleware\MiamiTokenAuthorization::class,
];
```

Apply the middleware to your desired route(s):

```php
Route::group(['middleware' => 'authman.miamioh:ApplicationName,CategoryName,key1|key2|keyn'], function() {
    ...
}
```

Where ApplicationName, CategoryName and the keys are the relevant values for authorizing the user via CAM.

Consumers of the resource must include a valid Miami token in the `bearer` format in the HTTP Authorization header of the request:

```
Authorization: Bearer abc123
```

The module will validate the token, extract the username and perform the configured authorization check. If the user is authorized for at least one of the given keys, the request will be authorized and continue. 

## Notes on Authorization Behaviors

Keys in CAM are stored at the application level which means it is not possible to get a list of all keys which **may** be used in a given category. This package resolves keys by getting a list of keys currently **used** in the given category. If a key is not used by at least one authorization in a category, the package will not know about it. The authorization methods return `false` for any keys which are not found.

The backend used by this package does not indicate application or category names which are not found. It simply returns no authorizations in those cases. If your authorizations are failing, be sure to check the exact spelling and case of the application, category and key names you use.

## Caching

The package fetches entire categories in a single request and caches the results. Authorization caching is enabled by default and uses the configured Laravel cache service. Cache entries are keyed by application, category and username.

The default cache lifetime is 3600 seconds (one hour). The cache lifetime can be changed with the `AUTHMAN_CACHE_SECONDS` environment variable. You should be aware, and ensure stakeholders are aware, that authorization changes in test and production may take up to the cache lifetime to apply. If an urgent change is required, Laravel `artisan` can be used to clear the cache:

```php
php artisan cache:clear
```

Note that if a local cache driver (such as `file`) is used, the `cache:clear` command must be run on each server.

During development, you may want to set your cache driver to `array` in order to ensure you are always working with the most up-to-date authorization data. 

## Testing Helpers

This package includes a trait to make testing easier. During tests, you don't want the package to actually make HTTP requests to the REST authorization service. Using the trait, you can set your desired authorization behaviors and the `AuthorizationService` object will respond appropriately.

**Note**: All authorizations are denied by default. You must explicitly allow authorizations.

```php
use MiamiOH\AuthMan\Testing\AuthorizeWithAuthMan;

class MainPageTest extends FeatureTestCase
{
    use AuthorizeWithAuthMan;

    public function testApplicationAuthorization()
    {
        // All authorizations are allowed
        $this->withAllAuthorizations();
        
        // Use the given authorization behaviors
        $this->withAuthorizations([
            'My Application' => [
                'My Category' => [
                    ['admin', false], // deny
                    ['user', true], // explicit allow 
                    ['guest'], // implicit allow
                ],
            ],
            ...
        ]);

        ...
    }
}
```

The `withAuthorizations` method accepts a nested array of applications, categories and authorizations. The authorization data array must conform to:

```php
[key, allowed]
```

The `key` is the expected authorization key to match. The `allowed` value is optional and if given, must be a boolean. The authorization's `allowed` value defaults to `true`. If a key is not explicitly listed, it will be interpreted as `false`.

## Running Unit Tests

This package uses Illuminate packages and must be tested with different versions for maximum compatibility. In most cases, the Illuminate package maintainers are able to provide forward and backward compatibility. This package is known to work with Illuminate 6 through 11. This range covers different versions of PHP and requires a matrix approach to testing. Manually executing the tests is possible using the includes `tests.sh` shell script in conjunction with our PHP build images.

A prerequisite for running the tests is being able to run the necessary containers. Once you have installed Docker Desktop (or the container platform of your choice), make sure you can run:

```bash
docker run -it --rm -v $(pwd):/opt/project -v ~/.composer:/root/.composer miamioh/php:7.3-devtools bash
```

This will bring to a shell in the PHP 7.3 image which is suitable for running tests. Exit the shell and run the same command with the PHP 8.1 or 8.2 image.

Now use the appropriate version of PHP to run the package tests with different versions of Illuminate:

```bash
# PHP 7.3

bash tests.sh --illuminate 8.0 

# PHP 8.1

bash tests.sh --illuminate 8.0
bash tests.sh --illuminate 9.0 
bash tests.sh --illuminate 10.0 

# PHP 8.2
bash tests.sh --illuminate 11.0
```

**!!IMPORTANT!!** The test script alters the `composer.json` file as it executes tests with different versions. The script backs up the original and restores it after composer runs, but you must be careful not to commit changes which may leak. Carefully review any changes to the `composer.json` file before committing them.
