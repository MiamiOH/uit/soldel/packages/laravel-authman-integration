<?php

return [
    'default-application' => env('AUTHMAN_DEFAULT_APPLICATION', ''),
    'default-category' => env('AUTHMAN_DEFAULT_CATEGORY', ''),

    'cache-seconds' => env('AUTHMAN_CACHE_SECONDS', 3600),
];
