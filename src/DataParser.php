<?php

namespace MiamiOH\AuthMan;

use Illuminate\Support\Collection;

class DataParser
{
    public function categoryFromData(array $data): Collection
    {
        return array_reduce($data, function (Collection $c, array $authData) {
            $c->put($authData['key'], Authorization::fromDataArray($authData));
            return $c;
        }, new Collection());
    }
}
