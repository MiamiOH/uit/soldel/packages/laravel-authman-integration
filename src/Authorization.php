<?php

namespace MiamiOH\AuthMan;

class Authorization
{
    /**
     * @var string
     */
    private $application;
    /**
     * @var string
     */
    private $category;
    /**
     * @var string
     */
    private $key;
    /**
     * @var bool
     */
    private $allowed;

    public function __construct(
        string $application,
        string $category,
        string $key,
        bool   $allowed
    ) {
        $this->application = $application;
        $this->category = $category;
        $this->key = $key;
        $this->allowed = $allowed;
    }

    public static function fromDataArray(array $data): self
    {
        return new self(
            $data['applicationName'],
            $data['module'],
            $data['key'],
            filter_var($data['allowed'], FILTER_VALIDATE_BOOLEAN),
        );
    }

    public function application(): string
    {
        return $this->application;
    }

    public function category(): string
    {
        return $this->category;
    }

    public function key(): string
    {
        return $this->key;
    }

    public function allowed(): bool
    {
        return $this->allowed;
    }
}
