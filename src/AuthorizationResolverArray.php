<?php

namespace MiamiOH\AuthMan;

use Illuminate\Support\Collection;

class AuthorizationResolverArray extends AuthorizationResolver
{
    private $withAllAuthorizations = false;

    /** @var Collection */
    private $withAuthorizations;

    private $queriedKey;

    public function __construct()
    {
        $this->withAuthorizations = new Collection();
    }

    /**
     * @codeCoverageIgnore
     */
    public function withoutCache(): AuthorizationResolver
    {
        return $this;
    }

    public function withAllAuthorizations(): void
    {
        $this->withAllAuthorizations = true;
    }

    public function withAuthorizations(array $authorizations): void
    {
        $this->withAuthorizations = $this->parseAuthorizations($authorizations);
    }

    /**
     * @inheritdoc
     */
    public function getAuthorization(string $username, string $application, string $category, string $key): Authorization
    {
        $this->queriedKey = $key;

        return parent::getAuthorization($username, $application, $category, $key);
    }

    protected function getCategory(string $username, string $application, string $category): Collection
    {
        if ($this->hasAuthorizations()) {
            return $this->withAuthorizations;
        }

        return new Collection([
            $this->queriedKey => new Authorization(
                $application,
                $category,
                $this->queriedKey,
                $this->withAllAuthorizations
            )
        ]);
    }

    private function parseAuthorizations(array $authorizations): Collection
    {
        return array_reduce(array_keys($authorizations), function (Collection $c, string $application) use ($authorizations) {
            array_map(function (string $category) use ($c, $application, $authorizations) {
                foreach ($authorizations[$application][$category] as $authData) {
                    $c->put($authData[0], new Authorization(
                        $application,
                        $category,
                        $authData[0],
                        $authData[1] ?? true,
                    ));
                }
            }, array_keys($authorizations[$application]));

            return $c;
        }, new Collection());
    }

    private function hasAuthorizations(): bool
    {
        return $this->withAuthorizations->isNotEmpty();
    }
}
