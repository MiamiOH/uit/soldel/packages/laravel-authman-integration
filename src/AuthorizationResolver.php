<?php

namespace MiamiOH\AuthMan;

use Illuminate\Support\Collection;
use MiamiOH\AuthMan\Exceptions\ItemNotFoundException;

abstract class AuthorizationResolver
{
    public const AUTHORIZATION_CACHE_KEY_FORMAT = 'authman-category: %s / %s / %s';

    abstract public function withoutCache(): AuthorizationResolver;

    /**
     * @param string $username
     * @param string $application
     * @param string $category
     * @param string $key
     * @return Authorization
     * @throws ItemNotFoundException
     */
    public function getAuthorization(string $username, string $application, string $category, string $key): Authorization
    {
        $items = $this->getCategory($username, $application, $category);

        if ($items->has($key)) {
            return $items->get($key);
        }

        throw new ItemNotFoundException(sprintf('Request item %s was not found in %s -> %s', $key, $application, $category));
    }

    public function getAuthorizations(string $username, string $application, string $category): Collection
    {
        return $this->getCategory($username, $application, $category);
    }

    public function categoryCacheKey(string $username, string $application, string $category): string
    {
        return sprintf(self::AUTHORIZATION_CACHE_KEY_FORMAT, $application, $category, $username);
    }

    abstract protected function getCategory(string $username, string $application, string $category): Collection;
}
