<?php

namespace MiamiOH\AuthMan;

use Carbon\Carbon;

class Token
{
    /**
     * @var string
     */
    private $token;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $credentialSource;
    /**
     * @var Carbon
     */
    private $expires;

    public function __construct(string $token, string $username, string $credentialSource, Carbon $expires)
    {
        $this->token = $token;
        $this->username = $username;
        $this->credentialSource = $credentialSource;
        $this->expires = $expires;
    }

    public static function fromResponseData(array $data): self
    {
        $token = $data['token'];
        $username = $data['username'];
        $credentialSource = $data['credential_source'];
        $expires = Carbon::parse($data['expiration_time'], 'America/New_York');

        return new self($token, $username, $credentialSource, $expires);
    }

    public function token(): string
    {
        return $this->token;
    }

    public function username(): string
    {
        return $this->username;
    }

    public function credentialSource(): string
    {
        return $this->credentialSource;
    }

    public function expirationTime(): Carbon
    {
        return $this->expires;
    }

    public function isValid(): bool
    {
        return $this->expires->gt(Carbon::now());
    }

    public function isExpired(): bool
    {
        return !$this->isValid();
    }
}
