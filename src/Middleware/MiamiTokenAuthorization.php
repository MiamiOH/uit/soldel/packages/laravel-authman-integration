<?php

namespace MiamiOH\AuthMan\Middleware;

use Closure;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use MiamiOH\AuthMan\AuthorizationService;
use MiamiOH\AuthMan\Exceptions\InvalidTokenException;
use MiamiOH\AuthMan\Exceptions\NoAuthorizationHeaderException;
use MiamiOH\AuthMan\TokenService;

class MiamiTokenAuthorization
{
    /** @var Guard */
    protected $auth;
    /**
     * @var TokenService
     */
    private $tokenService;
    /**
     * @var AuthorizationService
     */
    private $authorizationService;

    /**
     * MiamiTokenAuthentication constructor.
     * @param Guard $auth
     */
    public function __construct(Guard $auth, TokenService $tokenService, AuthorizationService $authorizationService)
    {
        $this->auth = $auth;
        $this->tokenService = $tokenService;
        $this->authorizationService = $authorizationService;
    }

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param Closure $next
     * @param string $applicationName
     * @param string $moduleName
     * @param string $keyList
     * @return mixed
     * @throws \JsonException
     * @throws \MiamiOH\RESTng\Client\RestNgClientException
     */
    public function handle(Request $request, Closure $next, string $applicationName, string $moduleName, string $keyList)
    {
        $this->authorizationService->clearAuthenticatedTokenUser();

        try {
            $token = $this->tokenService->resolveTokenFromRequest($request);
        } catch (NoAuthorizationHeaderException|InvalidTokenException $e) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        // TODO add token to cache if enabled

        if ($token->isExpired()) {
            return new Response(null, Response::HTTP_UNAUTHORIZED);
        }

        $category = $this->authorizationService->fromApplication($applicationName)->fromCategory($moduleName);

        if ($category->isAuthorizedAny($token->username(), explode('|', $keyList))) {
            $this->authorizationService->setAuthenticatedTokenUser($token->username());
            return $next($request);
        }

        return new Response(null, Response::HTTP_UNAUTHORIZED);
    }
}
