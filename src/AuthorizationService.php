<?php

namespace MiamiOH\AuthMan;

use Illuminate\Config\Repository as AppConfig;
use Illuminate\Support\Facades\Log;

class AuthorizationService
{
    /** @var string|null */
    private static $authenticatedTokenUser;
    /**
     * @var AuthorizationResolver
     */
    private $authorizationResolver;
    /**
     * @var AppConfig
     */
    private $appConfig;
    /**
     * @var string
     */
    private $application;
    /**
     * @var string
     */
    private $category;
    /**
     * @var bool
     */
    private $disableCache;

    public function __construct(
        AuthorizationResolver $authorizationResolver,
        AppConfig $appConfig,
        string $application = null,
        string $category = null,
        bool $disableCache = false
    ) {
        $this->authorizationResolver = $authorizationResolver;
        $this->appConfig = $appConfig;

        $this->application = $application ?? $appConfig->get('authman.default-application');
        $this->category = $category ?? $appConfig->get('authman.default-category');

        $this->disableCache = $disableCache;
    }

    public function clearAuthenticatedTokenUser(): void
    {
        self::$authenticatedTokenUser = null;
    }

    public function setAuthenticatedTokenUser(string $user): void
    {
        self::$authenticatedTokenUser = $user;
    }

    public function authenticatedTokenUser(): ?string
    {
        return self::$authenticatedTokenUser;
    }

    public function fromApplication(string $application): self
    {
        return new self($this->authorizationResolver, $this->appConfig, $application, $this->category, $this->disableCache);
    }

    public function fromCategory(string $category): self
    {
        return new self($this->authorizationResolver, $this->appConfig, $this->application, $category, $this->disableCache);
    }

    public function withoutCache(): self
    {
        return new self($this->authorizationResolver, $this->appConfig, $this->application, $this->category, true);
    }

    /**
     * The isAuthorized method is appropriate for requests which may perform multiple
     * authorization checks since this loads and keeps all auths for the user.
     *
     * @param string $userName
     * @param string $key
     * @return bool
     */
    public function isAuthorized(string $userName, string $key): bool
    {
        try {
            $authorization = $this->getResolver()->getAuthorization($userName, $this->application, $this->category, $key);
        } catch (Exceptions\ItemNotFoundException $e) {
            Log::info(sprintf('The key %s was not found used in %s / %s, assuming not authorized', $key, $this->application, $this->category));
            return false;
        }

        return $authorization->allowed();
    }

    public function isAuthorizedAny(string $userName, array $keys): bool
    {
        $authorizations = $this->getResolver()->getAuthorizations($userName, $this->application, $this->category);

        $allowed = $authorizations->filter(function (Authorization $authorization) use ($keys) {
            return $authorization->allowed() && in_array($authorization->key(), $keys, true);
        });

        return $allowed->isNotEmpty();
    }

    /**
     * @param string $userName
     * @return array
     */
    public function getAuthorizedKeys(string $userName): array
    {
        $authorizations = $this->getResolver()->getAuthorizations($userName, $this->application, $this->category);

        return $authorizations->reduce(function (array $c, Authorization $authorization) {
            if ($authorization->allowed()) {
                $c[] = $authorization->key();
            }

            return $c;
        }, []);
    }

    private function getResolver(): AuthorizationResolver
    {
        if ($this->disableCache) {
            return $this->authorizationResolver->withoutCache();
        }

        return $this->authorizationResolver;
    }
}
