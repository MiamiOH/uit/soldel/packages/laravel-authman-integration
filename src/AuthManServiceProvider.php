<?php

namespace MiamiOH\AuthMan;

use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

/**
 * Class LaravelAuthManServiceProvider
 * @package MiamiOH\AuthMan
 *
 * @codeCoverageIgnore
 */
class AuthManServiceProvider extends ServiceProvider
{
    private $configPath = __DIR__ . '/../config/authman.php';

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            $this->configPath => config_path('authman.php'),
        ]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(
            $this->configPath,
            'authman'
        );

        if (App::environment('testing')) {
            $this->app->singleton(AuthorizationResolverArray::class);
            $this->app->singleton(AuthorizationResolver::class, AuthorizationResolverArray::class);
        } else {
            $this->app->bind(AuthorizationResolver::class, AuthorizationResolverRest::class);
            $this->app->when(AuthorizationResolverRest::class)
                ->needs('$cacheSeconds')
                ->give(config('authman.cache-seconds'));
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return [AuthorizationService::class];
    }
}
