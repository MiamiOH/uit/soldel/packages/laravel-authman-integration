<?php

namespace MiamiOH\AuthMan;

use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use MiamiOH\AuthMan\Exceptions\ClientException;
use MiamiOH\RESTng\Client\Agent;

class AuthorizationResolverRest extends AuthorizationResolver
{
    public const AUTHORIZATION_API_BASE = '/api/authorization/v1';

    /**
     * @var Agent
     */
    private $agent;
    /**
     * @var DataParser
     */
    private $parser;
    /**
     * @var int
     */
    private $cacheSeconds;
    /**
     * @var bool
     */
    private $cacheEnabled;

    public function __construct(Agent $agent, DataParser $parser, int $cacheSeconds, bool $cacheEnabled = true)
    {
        $this->agent = $agent;
        $this->parser = $parser;
        $this->cacheSeconds = $cacheSeconds;
        $this->cacheEnabled = $cacheEnabled;
    }

    public function withoutCache(): AuthorizationResolver
    {
        return new self($this->agent, $this->parser, $this->cacheSeconds, false);
    }

    protected function getCategory(string $username, string $application, string $category): Collection
    {
        if ($this->cacheEnabled) {
            $this->refreshCategory($username, $application, $category);

            return Cache::get($this->categoryCacheKey($username, $application, $category));
        }

        return $this->getCategoryFromSource($username, $application, $category);
    }

    private function refreshCategory(string $username, string $application, string $category): void
    {
        $cacheKey = $this->categoryCacheKey($username, $application, $category);

        if (Cache::has($cacheKey)) {
            return;
        }

        Cache::remember($cacheKey, $this->cacheSeconds, function () use ($username, $application, $category) {
            return $this->getCategoryFromSource($username, $application, $category);
        });
    }

    /**
     * @param string $username
     * @param string $application
     * @param string $category
     * @return Collection
     * @throws ClientException
     * @throws \JsonException
     * @throws \MiamiOH\RESTng\Client\RestNgClientException
     */
    private function getCategoryFromSource(string $username, string $application, string $category): Collection
    {
        $response = $this->agent->run(
            new Request('GET', sprintf('%s/%s/%s?username=%s', self::AUTHORIZATION_API_BASE, $application, $category, $username))
        );

        if ($response->status() === 200) {
            return $this->parser->categoryFromData($response->data());
        }

        throw new ClientException(sprintf('Request for %s -> %s failed with %s', $application, $category, $response->status()));
    }
}
