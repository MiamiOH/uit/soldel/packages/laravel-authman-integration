<?php

namespace MiamiOH\AuthMan;

use GuzzleHttp\Psr7\Request as GuzzleRequest;
use Illuminate\Http\Request;
use MiamiOH\AuthMan\Exceptions\InvalidTokenException;
use MiamiOH\AuthMan\Exceptions\NoAuthorizationHeaderException;
use MiamiOH\RESTng\Client\Agent;

class TokenService
{
    public const AUTHENTICATION_RESOURCE = '/api/authentication/v1';

    /**
     * @var Agent
     */
    private $agent;

    public function __construct(Agent $agent)
    {
        $this->agent = $agent;
    }

    /**
     * @param Request $request
     * @return Token
     * @throws InvalidTokenException
     * @throws NoAuthorizationHeaderException
     * @throws \JsonException
     * @throws \MiamiOH\RESTng\Client\RestNgClientException
     */
    public function resolveTokenFromRequest(Request $request): Token
    {
        return $this->retrieveTokenData($this->getTokenFromRequest($request));
    }

    /**
     * @param Request $request
     * @return string
     * @throws NoAuthorizationHeaderException
     */
    private function getTokenFromRequest(Request $request): string
    {
        if (!$request->hasHeader('Authorization')) {
            throw new NoAuthorizationHeaderException('The request does not have an authorization header');
        }

        return preg_replace('/^(bearer|token)\s+/i', '', $request->header('Authorization'));
    }

    /**
     * @param string $token
     * @return Token
     * @throws InvalidTokenException
     * @throws \JsonException
     * @throws \MiamiOH\RESTng\Client\RestNgClientException
     */
    private function retrieveTokenData(string $token): Token
    {
        // TODO retrieve token from cache if cache enabled

        $response = $this->agent->run(
            new GuzzleRequest(
                'get',
                sprintf('%s/%s', self::AUTHENTICATION_RESOURCE, $token)
            )
        );

        if ($response->status() === 404) {
            throw new InvalidTokenException(sprintf('Token %s was not found', $token));
        }

        return Token::fromResponseData($response->data());
    }
}
