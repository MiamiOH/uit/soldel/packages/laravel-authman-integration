<?php

namespace MiamiOH\AuthMan\Exceptions;

class KeyNotFoundException extends AuthorizationException
{
}
