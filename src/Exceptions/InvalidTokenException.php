<?php

namespace MiamiOH\AuthMan\Exceptions;

class InvalidTokenException extends AuthorizationException
{
}
