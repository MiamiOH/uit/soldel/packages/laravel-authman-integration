<?php

namespace MiamiOH\AuthMan\Exceptions;

class ModuleNotFoundException extends AuthorizationException
{
}
