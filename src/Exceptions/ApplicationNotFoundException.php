<?php

namespace MiamiOH\AuthMan\Exceptions;

class ApplicationNotFoundException extends AuthorizationException
{
}
