<?php

namespace MiamiOH\AuthMan\Exceptions;

class ItemNotFoundException extends AuthorizationException
{
}
