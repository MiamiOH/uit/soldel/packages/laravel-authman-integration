<?php

namespace MiamiOH\AuthMan\Exceptions;

class NoAuthorizationHeaderException extends AuthorizationException
{
}
