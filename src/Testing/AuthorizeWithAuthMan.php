<?php

namespace MiamiOH\AuthMan\Testing;

use MiamiOH\AuthMan\AuthorizationResolverArray;

/**
 * Trait AuthorizeWithAuthMan
 * @package MiamiOH\AuthMan\Testing
 *
 * @codeCoverageIgnore
 */
trait AuthorizeWithAuthMan
{
    protected function withAllAuthorizations(): void
    {
        /** @var AuthorizationResolverArray $authResolver */
        $authResolver = $this->app->make(AuthorizationResolverArray::class);

        $authResolver->withAllAuthorizations();
    }

    protected function withoutAllAuthorizations(): void
    {
        /** @var AuthorizationResolverArray $authResolver */
        $authResolver = $this->app->make(AuthorizationResolverArray::class);

        $authResolver->withoutAllAuthorizations();
    }

    protected function withAuthorizations(array $authorizations): void
    {
        /** @var AuthorizationResolverArray $authResolver */
        $authResolver = $this->app->make(AuthorizationResolverArray::class);

        $authResolver->withAuthorizations($authorizations);
    }

    protected function withoutAuthorizations(array $authorizations): void
    {
        /** @var AuthorizationResolverArray $authResolver */
        $authResolver = $this->app->make(AuthorizationResolverArray::class);

        $authResolver->withOutAuthorizations($authorizations);
    }
}
